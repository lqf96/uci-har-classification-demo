class SGD(object):
    def __init__(self, parameters, lr):
        ## Network parameters
        self.parameters = parameters
        ## Learning rate
        self._lr = lr
    def step(self):
        # Adjust each parameter by respective derivative
        for name, param in self.parameters.items():
            param.data -= self._lr*param._grad
    def zero_grad(self):
        pass

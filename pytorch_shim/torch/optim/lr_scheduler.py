class StepLR(object):
    def __init__(self, optimizer, step_size, gamma=0.5):
        ## Optimizer
        self.optimizer = optimizer
        ## Step size
        self.step_size = step_size
        ## Gamma
        self.gamma = gamma
        ## Current steps
        self._current_step = 0
    def step(self):
        self._current_step += 1
        # Reduce learning rate
        if self._current_step%self.step_size==0:
            self.optimizer._lr *= self.gamma

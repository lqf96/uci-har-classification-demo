import numpy as np

import torch
from torch import _ShimTensor
from torch.autograd import Variable

class Module(object):
    def parameters(self):
        # No parameters by default
        return {}

class Sequential(Module):
    def __init__(self, *layers):
        ## Amount of layers
        n_layers = self._n_layers = len(layers)
        # Set layers
        for i in range(n_layers):
            setattr(self, str(i), layers[i])
    def parameters(self):
        params = {}
        # Aggragate parameters from each layer
        for i in range(self._n_layers):
            layer = getattr(self, str(i))
            for name, param in layer.parameters().items():
                params["%d.%s" % (i, name)] = param
        return params
    def __call__(self, variable):
        # Forward through network
        outputs = Variable(self._forward(variable.data))
        # Set previous layer
        outputs._prev = self
        return outputs
    def _forward(self, x):
        # Forward through each layer
        for i in range(self._n_layers):
            layer = getattr(self, str(i))
            x = layer._forward(x)
        return x
    def _backward(self, grad):
        grad_all_params = {}
        # Backward through each layer
        for i in range(self._n_layers-1, -1, -1):
            layer = getattr(self, str(i))
            grad = layer._backward(grad)
        return grad

class Linear(Module):
    def __init__(self, in_features, out_features):
        ## In features
        self.in_features = in_features
        ## Out features
        self.out_features = out_features
        ## Weight
        self._weight = Variable(_ShimTensor(np.random.normal(scale=0.01, size=(in_features, out_features))))
        ## Bias
        self._bias = Variable(_ShimTensor(np.zeros([1, out_features])))
    def parameters(self):
        # Weight and bias
        return {
            "weight": self._weight,
            "bias": self._bias
        }
    def _forward(self, x):
        self._x = x
        return torch.dot(x, self._weight.data)+self._bias.data
    def _backward(self, grad):
        # Gradient of weight and bias
        self._weight._grad = torch.dot(self._x.transpose(), grad)
        self._bias._grad = torch.dot(torch.ones([grad.size(0)]), grad)
        # Gradient of input
        return torch.dot(grad, self._weight.data.transpose())

class ReLU(Module):
    def _forward(self, x):
        mask = self._mask = (x>0).float()
        return x*mask
    def _backward(self, grad):
        return grad*self._mask

class _SoftmaxCrossEntropyLoss(object):
    def __call__(self, x, labels):
        # Save input
        self._x = x
        # Softmax
        softmax_out = (torch.exp(x.data).transpose()/torch.sum(torch.exp(x.data), 1).transpose()).transpose()
        # One-hot encoded labels
        self._one_hot_labels = one_hot_labels = torch._identity(6)[labels.data]
        # Calculate cross entropy and wrap result in variable
        cross_entropy = -torch.sum(one_hot_labels*torch.log(softmax_out), 1)
        # Wrap output in variable
        output = Variable(cross_entropy)
        output._prev = self
        return output
    def _backward(self):
        self._x._prev._backward(self._x.data-self._one_hot_labels)

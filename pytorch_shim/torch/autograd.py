from torch import _ShimTensor

class Variable(object):
    def __init__(self, data):
        assert isinstance(data, _ShimTensor)
        ## Data
        self.data = data
        ## Previous layer
        self._prev = None
        ## Gradient
        self._grad = None
    def backward(self):
        # Start gradient calculation from previous layer
        self._prev._backward()

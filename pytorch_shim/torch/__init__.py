import functools

import numpy as np

def _numpy_op_wrapper(numpy_func):
    @functools.wraps(numpy_func)
    def wrapper(*args, **kwargs):
        # Unwrap shim tensor as NumPy array
        args = [arg._data if isinstance(arg, _ShimTensor) else arg for arg in args]
        kwargs = dict(
            (name, arg._data) if isinstance(arg, _ShimTensor) else (name, arg)
            for kwarg in kwargs.items()
        )
        # Call NumPy function
        result = numpy_func(*args, **kwargs)
        # Wrap NumPy array as shim tensor
        if isinstance(result, np.ndarray):
            result = _ShimTensor(result)
        return result
    return wrapper

class _ShimTensor(object):
    def __init__(self, data):
        ## Wrapped data
        self._data = data
    # Shape of the tensor
    def size(self, dim=None):
        shape = self._data.shape
        if dim!=None:
            shape = shape[dim]
        return shape
    # Transpose
    def transpose(self):
        return _ShimTensor(self._data.T)
    # Convert to float tensor
    def float(self):
        return _ShimTensor(self._data.astype("float"))
    # Tensor operations
    __add__ = __radd__ = _numpy_op_wrapper(np.add)
    __sub__ = _numpy_op_wrapper(np.subtract)
    __rsub__ = _numpy_op_wrapper(np.ndarray.__rsub__)
    __mul__ = __rmul__ = _numpy_op_wrapper(np.multiply)
    __truediv__ = _numpy_op_wrapper(np.divide)
    __neg__ = _numpy_op_wrapper(np.negative)
    __eq__ = _numpy_op_wrapper(np.equal)
    __gt__ = _numpy_op_wrapper(np.greater)
    __getitem__ = _numpy_op_wrapper(np.ndarray.__getitem__)
    sum = _numpy_op_wrapper(np.sum)

# Tensor operations
dot = _numpy_op_wrapper(np.dot)
ones = _numpy_op_wrapper(np.ones)
log = _numpy_op_wrapper(np.log)
exp = _numpy_op_wrapper(np.exp)
sum = _numpy_op_wrapper(np.sum)
mean = _numpy_op_wrapper(np.mean)
# PyTorch Shim specific tensor operations
_max = _numpy_op_wrapper(np.max)
_argmax = _numpy_op_wrapper(np.argmax)
_identity = _numpy_op_wrapper(np.identity)
_maximum = _numpy_op_wrapper(np.maximum)

def from_numpy(ndarray):
    return _ShimTensor(ndarray)

def max(x, dim):
    return _max(x, dim), _argmax(x, dim)

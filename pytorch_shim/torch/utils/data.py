from collections.abc import Iterator
import numpy as np

import torch

class TensorDataset(object):
    def __init__(self, data_tensor, target_tensor):
        assert data_tensor.size(0)==target_tensor.size(0)
        ## Tensor with features
        self.data_tensor = data_tensor
        ## Tensor with labels
        self.target_tensor = target_tensor
    def __getitem__(self, index):
        return self.data_tensor[index], self.target_tensor[index]
    def __len__(self):
        return self.data_tensor.size(0)

class DataLoaderIter(Iterator):
    def __init__(self, loader):
        ## Data loader
        self.loader = loader
        ## Current batch number
        self._current_batch = 0
        # Dataset size
        dataset_size = len(loader.dataset)
        ## Number of batches
        n_batch = self._n_batch = dataset_size//loader.batch_size
        ## Index sequence
        index_seq = self._index_seq = np.array(range(dataset_size))
        if loader._shuffle:
            np.random.shuffle(index_seq)
    def __next__(self):
        # End of sequence
        if self._current_batch>=self._n_batch:
            raise StopIteration()
        # Get item indexes for current iteration
        loader = self.loader
        index_begin = loader.batch_size*self._current_batch
        index_end = index_begin+loader.batch_size
        index_iter = self._index_seq[index_begin:index_end]
        # Update current batch number
        self._current_batch += 1
        # Pick batch from dataset
        dataset = loader.dataset
        return dataset.data_tensor[index_iter], dataset.target_tensor[index_iter]

class DataLoader(object):
    def __init__(self, dataset, batch_size=1, shuffle=False):
        ## Dataset
        self.dataset = dataset
        ## Batch size
        self.batch_size = batch_size
        ## Shuffle
        self._shuffle = shuffle
    def __iter__(self):
        return DataLoaderIter(self)

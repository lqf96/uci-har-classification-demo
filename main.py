#! /usr/bin/env python3
import sys, os, pickle
from argparse import ArgumentParser

def main():
    """!
    @brief Program entry.
    """
    # Argument parser
    parser = ArgumentParser(description="UCI HAR classication demo using Feed-Forward Neural Networks.")
    parser.add_argument("-p", "--path", type=str, required=True, help="Path to datasets root.")
    parser.add_argument("-t", "--train", type=str, nargs="?", const="train", help="Train with given dataset.")
    parser.add_argument("-m", "--model", type=str, default="model.pkl", help="Path to save or load model.")
    parser.add_argument("-a", "--apply", type=str, nargs="?", const="test", help="Apply to and compare prediction with given dataset.")
    parser.add_argument("-s", "--use-shim", action="store_true", help="Use PyTorch Shim module instead of PyTorch itself.")
    # Parse arguments
    args = parser.parse_args()
    # Use PyTorch Shim
    if args.use_shim:
        # Project directory
        project_dir = os.path.dirname(os.path.abspath(__file__))
        # Shim directory
        shim_dir = os.path.join(project_dir, "pytorch_shim")
        # Insert shim directory to Python path
        sys.path.insert(0, shim_dir)
    # Import
    from torch_impl import ffnn2_f561, loader_f561, train_f561, test_f561
    # Model
    network = None
    # Prompt error if no mode is chosen
    if not args.train and not args.apply:
        print("Error: You must choose either training or applying mode!", file=sys.stderr)
        return 1
    # Hyper parameters
    INPUT_SIZE = 561
    HIDDEN_SIZE = 100
    NUM_CLASSES = 6
    NUM_EPOCHS = 100
    BATCH_SIZE = 64
    LEARNING_RATE = 0.004 if args.use_shim else 0.02
    STEP_SIZE = 2000
    # Training mode
    if args.train:
        # Train loader
        train_loader = loader_f561(
            dir_path=args.path,
            dataset_name=args.train,
            batch_size=BATCH_SIZE,
            shuffle=True
        )
        # Network
        network = ffnn2_f561(
            input_size=INPUT_SIZE,
            hidden_size=HIDDEN_SIZE,
            num_classes=NUM_CLASSES,
            use_shim=args.use_shim
        )
        # Train network
        train_f561(
            network=network,
            loader=train_loader,
            n_epochs=NUM_EPOCHS,
            batch_size=BATCH_SIZE,
            learning_rate=LEARNING_RATE,
            step_size=STEP_SIZE,
            use_shim=args.use_shim
        )
        # Save model to given path
        if args.model:
            with open(args.model, "wb") as f:
                pickle.dump(network, f)
    # Applying mode
    if args.apply:
        # Load network from file
        if not network:
            with open(args.model, "rb") as f:
                network = pickle.load(f)
        # Apply loader
        apply_loader = loader_f561(
            dir_path=args.path,
            dataset_name=args.apply,
            batch_size=BATCH_SIZE,
            shuffle=False
        )
        # Apply model to data
        accuracy = test_f561(
            network=network,
            loader=apply_loader,
        )
        print("Accuracy: %.2f%%" % accuracy)
    return 0

if __name__=="__main__":
    sys.exit(main())

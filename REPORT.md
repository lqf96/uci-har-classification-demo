# 云数据管理 第一次大作业 实验报告
软件41 鲁其璠 2014013423

## 算法描述
本次作业我利用两层的前馈神经网络对UCI HAR的561维特征执行了分类任务，网络各层依次为线性层、ReLU层、线性层、Softmax层，所用的损失函数为交叉熵，并使用梯度下降与反向传播算法对参数进行调整。

### 线性层
线性层对输入进行线性变换：

$$y = xW+b$$

反向传播求导时，输出对`W`,`b`与输入的偏导数分别为：

$$\frac{\partial y}{\partial W} = x$$

$$\frac{\partial y}{\partial b} = \mathbf{1}$$

$$\frac{\partial y}{\partial x} = W^T$$

### ReLU
ReLU变换的规则极为简单：大于0的项保持不变，而小于0的项则置为0：

$$y = x*\mathbf{1} (x>0)$$

相应地，对于大于0的项，ReLU的导数为1，小于0的项导数则为0:

$$\frac{\partial y}{\partial x} = \mathbf{1} (x>0)$$

由于ReLU在保证线性性的同时，其自身与其导数易于计算，因此ReLU现已广泛用于各类神经网络中。

### Softmax和交叉熵
Softmax对输入向量各维归一化：

$$y_i = \frac{exp(x_i)}{\sum_jexp(x_j)}$$

交叉熵则可以有效衡量当前输出与实际结果间的差距：

$$J = -\sum_it_ilog(y_i)$$

合并计算二者对输入的导数较为简单：

$\frac{\partial J}{\partial x_i} = \sum_i t_i\frac{\sum_jexp(x_j)}{(\sum_jexp(x_j)^2)}\frac{\partial \sum_jexp(x_j)}{\partial x_i} = y_i-t_i$

### 反向传播与梯度下降
本次作业采取最为简单的反向传播方法，即逐层计算梯度（导数）后，将各层参数沿梯度下降方向调整。目前采用的学习率为0.004 (PyTorch Shim)或0.02 (PyTorch)。由于网络迭代后期，各参数逐渐接近最优，为防止参数变化过于剧烈，从而离开极小区域，训练时会对网络的学习率进行调整，具体规则为每2000次迭代后，学习率将会减半。实际运行过程中，该调整对网络效果起到了重要的作用。

## 源码接口和实现描述
### PyTorch Shim
本工程的核心是PyTorch Shim。PyTorch Shim是参考PyTorch而完成的一个简易神经网络框架，它具有如下API，其中无下划线的API与PyTorch的相应API相兼容：
- `torch`
  * `_ShimTensor`：张量类，实际仅包裹了NumPy的多元数组对象，并提供与Torch中`Tensor`类相兼容的API，无其它作用。
  * `from_numpy`：将NumPy多维数组转换为张量实例。
  * 其余实用函数，它们包装了NumPy中的相应函数。
- `torch.autograd`
  * `Variable`：表示网络中的一个变量，变量除含有数据外，还含有反向传播过程对应的导数与指向创建该变量的模块的引用。
    + `backward()`：从当前变量开始反向传播，逐层计算导数。
- `torch.nn`
  * `Module`：模块类，表示神经网络的一部分结构。
    + `parameters()`：获取当前网络结构的所有参数。
    + `_forward()`：抽象方法，计算当前层网络的输出。
    + `_backward()`：抽象方法，反向传播过程中计算当前层的参数与输入的导数。
  * `Sequential`：表示顺序结构的神经网络。
    + `__init__()`：初始化顺序结构的神经网络，参数为网络各层。
    + `__call__()`：对输入变量逐层迭代计算输出。
  * `Linear`：表示一个线性层。
    + `__init__()`：初始化线性层，参数为输入维度和输出维度。
  * `ReLU`：表示一个ReLU层。
  * `_SoftmaxCrossEntropyLoss`：表示结合了`Softmax`层的交叉熵损失函数。
- `torch.optim`
  * `SGD`：随机梯度下降类，用于根据反向传播过程计算出的导数调整神经网络参数。
    + `step()`：利用计算出的各层导数，对网络参数进行调整。
- `torch.util.data`
  * `Dataset`：数据集类，包括了分类任务中的特征向量与标签向量。
    + `__getitem__()`：获取数据集中的某一项。
    + `__len__()`：获取数据集大小。
  * `DataLoader`：数据加载器类（PyTorch Shim中仅包装了相应数据集，无其它作用）
    + `__init__()`：初始化数据加载器，参数为数据集、每批数据的大小以及是否按随机顺序返回数据。
    + `__iter__()`：返回数据集迭代器。
  * `DataLoaderIterator`：数据加载器迭代器类，负责每次顺序或随机地返回一批数据(Batch)供训练。
    + `__next__()`：返回下一批数据，包含特征与标签。

### 训练代码
网络的训练代码位于`torch_impl.py`中，其中包括了如下函数：
* `loader_f561()`：加载UCI HAR 561维特征数据集。
* `ffnn2_f561()`：返回本项目中的两层神经网络结构。
* `train_f561()`：使用给定数据集训练网络。
* `test_f561()`：使用给定数据集对网络检验准确率。

训练程序的入口点位于`main.py`中，用法如下：
```sh
usage: main.py [-h] -p PATH [-t [TRAIN]] [-m MODEL] [-a [APPLY]] [-s]
```

其中各选项的含义如下：
* `-p`：UCI HAR数据集根文件夹路径（如`/Users/lqf/Playground/uci_har_dataset`）
* `-t [TRAIN]`：使用给定的子文件夹中的数据训练网络（默认为`train`）
* `-a [APPLY]`：使用给定的子文件夹中的数据检验网络（默认为`test`）
* `-m MODEL`：训练模式下，存储训练好的模型到给定位置；仅检验模式下，从给定位置加载训练好的模型。
* `-s`：是否使用PyTorch Shim。使用PyTorch Shim时，程序仅依赖于NumPy，并使用自带的神经网络实现。不使用PyTorch Shim时，程序需依赖于PyTorch，使用PyTorch的神经网络实现。注意两种模式下训练出的模型不能交叉使用。

例如，若需要训练网络并保存训练结果，可以运行：

```sh
./main.py [-s] -p /path/to/uci_har_dataset -t -m /path/to/model.pkl
```

若只需要加载已经训练好的模型并检验模型效果，则可运行：

```sh
./main.py [-s] -p /path/to/uci_har_dataset -a -m /path/to/model.pkl
```

注意本工程只与Python 3兼容，不支持Python 2。

## 实验结果
仅依赖NumPy，使用PyTorch Shim训练网络，最高准确率为96.30%（模型文件为`model-shim.pkl`）。使用PyTorch训练网络，最高准确率为93.82%（模型文件为`model-torch.pkl`）。

import os

import numpy as np, torch, torch.nn
from torch.nn import Module, Sequential, Linear, ReLU
from torch.optim import SGD
from torch.optim.lr_scheduler import StepLR
from torch.autograd import Variable
from torch.utils.data import TensorDataset, DataLoader

def loader_f561(dir_path, dataset_name, batch_size, shuffle):
    # Feature and label file path
    features_path = os.path.join(dir_path, dataset_name, "X_%s.txt" % dataset_name)
    labels_path = os.path.join(dir_path, dataset_name, "y_%s.txt" % dataset_name)
    # Data
    features = torch.from_numpy(np.loadtxt(features_path, dtype="float32"))
    labels = torch.from_numpy(np.loadtxt(labels_path, dtype="long")-1)
    # Dataset
    f561_dataset = TensorDataset(data_tensor=features, target_tensor=labels)
    # Data loader
    return DataLoader(
        dataset=f561_dataset,
        batch_size=batch_size,
        shuffle=shuffle
    )

def ffnn2_f561(input_size, hidden_size, num_classes, use_shim):
    layers = [
        Linear(input_size, hidden_size),
        ReLU(),
        Linear(hidden_size, num_classes)
    ]
    # Separated layer when using real Torch
    if not use_shim:
        layers.append(torch.nn.Softmax())
    return Sequential(*layers)

def train_f561(network, loader, n_epochs, batch_size, learning_rate, step_size, use_shim):
    # Loss and optimizer
    criterion = torch.nn._SoftmaxCrossEntropyLoss() if use_shim else torch.nn.CrossEntropyLoss()
    optimizer = SGD(network.parameters(), lr=learning_rate)
    # Number of steps
    n_steps = len(loader.dataset)//batch_size
    # Step learning rate
    scheduler = StepLR(optimizer, step_size=step_size, gamma=0.5)
    # Train the model
    for epoch in range(n_epochs):
        for i, (inputs, labels) in enumerate(loader):
            # Convert tensor to variable
            inputs = Variable(inputs)
            labels = Variable(labels)
            # Forward
            optimizer.zero_grad()
            outputs = network(inputs)
            # Backward
            loss = criterion(outputs, labels)
            loss.backward()
            # Adjust learning rate
            scheduler.step()
            # Optimize
            optimizer.step()
            # Print current progress
            if (i+1)%10==0:
                print("Epoch [%d/%d], Step [%d/%d], Loss: %.6f"
                    % (epoch+1, n_epochs, i+1, n_steps, torch.mean(loss.data)))

def test_f561(network, loader):
    # Correct predictions and total tests
    correct = 0
    total = 0
    # Test dataset
    for inputs, labels in loader:
        # Prediction
        inputs = Variable(inputs)
        outputs = network(inputs)
        _, predicted = torch.max(outputs.data, 1)
        # Update correct predictions and total tests
        correct += (predicted==labels).sum()
        total += labels.size(0)
    # Accuracy
    return 100*correct/total

# UCI HAR Classification Demo
UCI HAR classification using two-layer feed-forward neural network.

## Project Design
Network design:
```
Input -> Linear -> ReLU -> Linear -> Softmax -> Cross Entropy Loss
```
For thorough project design, see [report](REPORT.md).

## Dependencies
This project depends on NumPy and optionally depends on PyTorch when shim mode is not used.

## CLI Usage
```sh
usage: main.py [-h] -p PATH [-t [TRAIN]] [-m MODEL] [-a [APPLY]] [-s]
```
* `-p`: Path to UCI HAR dataset root directory.
* `-t`: Train network with given dataset. (Default: `train`)
* `-a`: Test network with given dataset. (Default: `test`)
* `-m`: Path to save or load trained model.
* `-s`: Use PyTorch Shim instead of PyTorch.

## License
[MIT License](LICENSE)
